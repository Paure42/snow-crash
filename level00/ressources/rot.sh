#!/usr/bin/env bash
set -euo pipefail

comp='A-Za-z'
rot1='B-ZAb-za'
rot2='C-ZA-Bc-za-b'
rot3='D-ZA-Cd-za-c'
rot4='E-ZA-De-za-d'
rot5='F-ZA-Ef-za-e'
rot6='G-ZA-Fg-za-f'
rot7='H-ZA-Gh-za-g'
rot8='I-ZA-Hi-za-h'
rot9='J-ZA-Ij-za-i'
rot10='K-ZA-Jk-za-j'
rot11='L-ZA-Kl-za-k'
rot12='M-ZA-Lm-za-l'
rot13='N-ZA-Mn-za-m'
rot14='O-ZA-No-za-n'
rot15='P-ZA-Op-za-o'
rot16='Q-ZA-Pq-za-p'
rot17='R-ZA-Qr-za-q'
rot18='S-ZA-Rs-za-r'
rot19='T-ZA-St-za-s'
rot20='U-ZA-Tu-za-t'
rot21='V-ZA-Uv-za-u'
rot22='W-ZA-Vw-za-v'
rot23='X-ZA-Wx-za-w'
rot24='Y-ZA-Xy-za-x'
rot25='ZA-Yza-y'
arr=($rot1 $rot2 $rot3 $rot4 $rot5 $rot6 $rot7 $rot8 $rot9 $rot10
     $rot11 $rot12 $rot13 $rot14 $rot15 $rot16 $rot17 $rot18 $rot19 $rot20
     $rot21 $rot22 $rot23 $rot24 $rot25);

if [ "$#" -ne '1' ]; then
    echo 'ERROR: Usage ./rot.sh <token>'
    exit 1
fi

if [[ "$1" =~ ^[a-zA-Z]+$ ]]; then
    for ((inc=1; inc < 25; inc++)); do
        decode=`tr "$comp" "${arr[inc]}" <<< "$1"`
        echo "ROT $inc : $decode"
    done
else
    echo 'ERROR: token must contain only letters'
    exit 1
fi
