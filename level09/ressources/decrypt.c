#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>

int main(int ac, char **av)
{
    FILE    *fs;
    char   *token = NULL;
    ssize_t token_size = 0;
    size_t buf_size = 0;

    if (ac != 2)
    {
        write(2, "ERROR: Usage ./decrypt <token>\n", strlen("ERROR: Usage ./decrypt <file>\n"));
        return(1);
    }
    if ((fs = fopen(av[1], "r")) == NULL)
        return(1);
   token_size =  getline(&token, &buf_size, fs);
    for(size_t i=0; i < token_size; i++)
    {
      if (token[i] != '\n')
      {
        char c = token[i] - i;
        write(1, &c, 1);
      }
    }
    write(1, "\n", 1);
    free(token);
    fclose(fs);
    return (0);
}
